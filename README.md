This repository includes custom code used to process data for the paper:
    
    Andrei Papkou, Jessica Hedge, Natalia Kapel, Bernadette Young and, R. Craig MacLean. **Efflux pump activity potentiates the evolution of antibiotic resistance across S. aureus isolates**. *Nature Communications* (2020)
    

Files:

## 1. Genomic analysis

**SA evolvability analysis pipeline.ipynb**  - a Jupyter notebook showing all steps for processing next generation sequencing data including


    1. The analysis of Illumina reads for the genomes of parental and evolved S.aureus strains 

    2. The analysis of dN/dS ratio in the evolved populations

    3. The analysis of transcriptome data for the parental strains

 
## 2. Image analysis

**count_coloies.ijm** - a small script written in ImageJ macro language to estimate a number of bacterial colonies using images of agar plates

Folder **sample_images**  contains sample images. 


To use **count_coloies.ijm** script you need [ImageJ2](https://downloads.imagej.net/) or[ Fiji](https://fiji.sc/) installed. Run the following command with 4 arguments after script's file name:

```
/path_to_imageJ_binary/ImageJ --ij2  count_coloies.ijm  INPUT_DIRECTORY OUTPUT_DIRECTORY THRESHOLD_VALUE MIN_COLONY_SIZE
```

where *INPUT_DIRECTORY* is a directory with image files in png format.

*OUTPUT_DIRECTORY* is a directory for storing files (please make sure it exist before running macro).

*THRESHOLD* a lower threshold value for converting an image into binary. The optimal value depends on image aquisition settings and should be determined empirically.

*MIN_COLONY_SIZE* a minimal colony size as an area measured in squre pixels. Depends on the size of your image and actual size of your colonies. 

For example on my PC I run:


```
# download repository

git clone https://gitlab.com/apson/sa_evolvability.git 

# change to the directory
cd sa_evolvability 

# create output directory 
mkdir out_images

# analysing images. please note that full paths are used and that the arguments for the macro are enclosed in double quotes

fiji --ij2  -macro count_colonies.ijm "/home/username/Downloads/sa_evolvability/sample_images /home/username/Downloads/sa_evolvability/out_images 140 40"
```

This will create a result file "Results.csv" listing image names and the number of colonies as first two columns. In addition, the script will copy the original images to the output directory and highlight counted colonies with outlines. This is convenient for fine-tuning parameters for different bathes of images.

Please note, that there are other important parameters that are hard-coded including file extension ("png"), upper threshold, maximum particle size, and plate location within the image. Please modify the script to your needs. 

Please also note that this script is sensitive to a light gradient (shading). Make sure the light is distributed regulary during image aquisition (e.g. have several sources of ligth)
