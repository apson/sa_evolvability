setBatchMode(true);
// getting arguments
arg = getArgument();
print("Running batch analysis with arguments:");
arguments=split(arg, ' ');
dir_in=arguments[0];
dir_out=arguments[1];
thresh=arguments[2];
minSize=arguments[3];
print("input directory " + dir_in);
print("output directory " + dir_out);
print("threshould " + thresh);
print("minimal colony size (area) " + minSize);



file_list = getFileList(dir_in);
for (k=0; k<file_list.length; k++){
    filename = dir_in+File.separator+file_list[k];
        if (endsWith(filename, "png")) {
        print("openning " + filename);
        open(filename);
        print("removing outliers and convert to 8 bit");
        run("Remove Outliers...", "radius=5 threshold=2 which=Bright");
        run("8-bit");
        print("select plate area to analyse ");
        makeOval(210, 0, 1210, 1190);
        run("Clear Outside");
        //convert image into binary
        setThreshold(thresh, 255);
        run("Make Binary");
        run("Erode");
        run("Watershed");
        //count colonies
        run("Analyze Particles...", "size="+minSize+"-1800 circularity=0.65-1.00 show=Overlay display summarize");
        // overlay colony counts on the original image and save file
        run("To ROI Manager");
        run("Revert");
        run("From ROI Manager");
        run("Flatten");
        saveAs("png", dir_out+File.separator+file_list[k]);
        close();
        close();
        }
}

//save the results
selectWindow("Summary");
saveAs("results", dir_out+File.separator+"Results.csv");
run("Close");
selectWindow("Results");
run("Close");


eval("script", "System.exit(0);");
